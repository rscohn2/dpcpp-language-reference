==========================
 DPC++ Language Reference
==========================
 
.. image:: https://gitlab.devtools.intel.com/infodev/dpcpp-language-reference/badges/master/pipeline.svg
    :target: https://gitlab.devtools.intel.com/infodev/dpcpp-language-reference/-/jobs
    :alt: pipeline status

Latest `HTML
<https://infodev.gitlab-pages.devtools.intel.com/dpcpp-language-reference/>`__
of master branch.

Contributors and reviewers may submit a pull request, comment on an
existing merge request, or file an issue to provide input.

Contact `Robert Cohn <mailto:robert.s.cohn@intel.com>`__ and `Jacqui
DuPont <mailto:jacqui.b.dupont@intel.com>`__ with questions.
