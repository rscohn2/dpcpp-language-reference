============
 class_name
============

::

   class class_name;

Description

.. rubric:: Template parameters

| ``T`` -

.. rubric:: Member types

========  =======
typea
typeb
========  =======

.. rubric:: Member functions

===============  =======
(constructors)_
functiona_
===============  =======

.. rubric:: Nonmember functions

===============  =======
functionb_
===============  =======

(constructors)
==============

functiona
=========

functionb
=========
