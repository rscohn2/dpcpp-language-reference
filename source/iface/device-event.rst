.. rst-class:: api-class
	       
==============
 device_event
==============

::

   class device_event;

.. rubric:: Member functions

==============  ===
wait_
==============  ===

wait
====

::

   void wait();

